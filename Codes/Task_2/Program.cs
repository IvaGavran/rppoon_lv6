﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Box box = new Box();
            box.AddProduct(new Product("First product", 150.00));
            box.AddProduct(new Product("Second product", 200.00));
            box.AddProduct(new Product("Third product", 100.00));


            IAbstractIterator iterator = box.GetIterator();

            for (Product product = iterator.First(); iterator.IsDone == false; product = iterator.Next())
            {
                Console.WriteLine(product.ToString());
            }         
        }
    }
}
