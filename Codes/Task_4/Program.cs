﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_4
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount account = new BankAccount("Iva Ivić", "Teslina 7", 10000);
            BankSystem system = new BankSystem();
            system.Memento = account.StoreState();
            Console.WriteLine(account);

            account.ChangeOwnerAddress("Cesarićeva 17");
            account.UpdateBalance(5000);
            Console.WriteLine(account);

            account.RestoreState(system.Memento);
            Console.WriteLine(account);
        }
    }
}
