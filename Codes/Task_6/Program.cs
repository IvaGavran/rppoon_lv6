﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_6
{
    class Program
    {
        static void Main(string[] args)
        {
            StringChecker checkdigit = new StringDigitChecker();
            string password = "123word";
            Console.WriteLine(checkdigit.Check(password));
            StringChecker checkupper = new StringUpperCaseChecker();
            Console.WriteLine(checkupper.Check(password));
            StringChecker checklower = new StringLowerCaseChecker();
            Console.WriteLine(checklower.Check(password));
            StringDigitChecker checkDigit = new StringDigitChecker();
            Console.WriteLine(checkDigit.Check(password));
            StringLengthChecker checkLength = new StringLengthChecker();
            Console.WriteLine(checkLength.Check(password));
        }
    }
}
