﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();
            notebook.AddNote(new Note("First title", "First text."));
            notebook.AddNote(new Note("Second title", "Second text."));
            notebook.AddNote(new Note("Third title", "Third text."));


            IAbstractIterator iterator = notebook.GetIterator();

            do
            {
                iterator.Current.Show();
                iterator.Next();
            } while (iterator.IsDone != true) ;
        }
    }
}
