﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_3
{ 
    class CareTaker
    {
        private List<Memento> previousState;
        private int count;
        public CareTaker()
        {
            this.previousState = new List<Memento>();
            this.count = previousState.Count - 1; }
        public CareTaker(List<Memento> previousState)
        {
            this.previousState = previousState;
            this.count = previousState.Count - 1;
        }
        public void AddPreviousState(Memento state)
        {
            previousState.Add(state);
            count = previousState.Count - 1;
        }
        public Memento Remove()
        {
            count = count - 1;
            if (count < 0)
                return null;
            return previousState[count];
        }
        public Memento Repeat()
        {
            count = count + 1;
            if (count > previousState.Count - 1)
                return null;
            return previousState[count];
        }
    }
}
