﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_3
{
    class Program
    {
        static void Main(string[] args)
        {
            CareTaker careTaker = new CareTaker();
            ToDoItem toDo = new ToDoItem("My title", "My text", DateTime.Now);
            careTaker.AddPreviousState(toDo.StoreState());

            toDo.Rename("New title");
            toDo.ChangeTask( "New task");
            Console.WriteLine("New:");
            careTaker.AddPreviousState(toDo.StoreState());
            Console.WriteLine(toDo.ToString());

            Console.WriteLine("Previous:");
            toDo.RestoreState(careTaker.Remove());
            Console.WriteLine(toDo.ToString());

            Console.WriteLine("New one redone:");
            toDo.RestoreState(careTaker.Repeat());
            Console.WriteLine(toDo.ToString());
        }
    }
}
