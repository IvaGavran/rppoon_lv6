﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_5
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, "logFile.txt");
            fileLogger.Log("Watch out", MessageType.WARNING);        
            logger.Log("This is error.", MessageType.ERROR);
            logger.Log("Information", MessageType.INFO);
            logger.SetNextLogger(fileLogger);
        }
    }
}
